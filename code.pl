% ===================== Laboratorio 2: LTC ==========================
%	Integrantes: Esteban Abarca
%				 Patricio Jara
% 	Profesor: 	 Rodrigo Pizarro
%	Ayudantes:	 Giselle Segovia
%				 Nicolas Uribe	
%	Fecha:		 Agosto 23, 2013
% ===================================================================
%Errores.
%Error1: Un bloque en la misma posici�n de otro bloque.					LISTO
%Error2: Un acido en la misma posician que otro acido.					LISTO
%Error3: Un bloque o �cido fuera de los rangos de la bodega.			LISTO
%Error4: Un bloque posicionado en el rango de restriccion del �cido	
%Error5: El archivo contiene m�s de una dimensi�n.						LISTO

% =========================== ERROR 1 ===============================
error1:-
	%Crea una lista de listas de dos elementos (Coordenadas (X,Y))
	%luego utiliza sort para eliminar los replicados y guarda la lista
	%limpia en otra variable, as� si sus tama�os son diferentes
	%significa que habian coordenadas iguales, entonces lanza error 1
    findall([A,B],bloque(A,B), L), %Crea una lista con todas los pares (X,Y)
    sort(L, R),					   %Elimina replicados
    length(L, NUM1),			   %Obtiene tama�os de la listas
    length(R, NUM2),			   %
    NUM1 =:= NUM2 -> true; 		   %Compara si son o no iguales (hay o
								   %no bloques repetidos)
	%como se detecto error, se da cuenta de ello en el archivo de
	%salida y se aborta la ejecucion.
	tell('salida.txt'),
	write('Error 1: Un bloque en la misma posicion que otro bloque.'),
	told,
	abort.


% ===================================================================

% =========================== ERROR 2 ===============================
error2:-
	
	%Crea una lista de listas de dos elementos (Coordenadas (X,Y))
	%luego utiliza sort para eliminar los replicados y guarda la lista
	%limpia en otra variable, as� si sus tama�os son diferentes
	%significa que habian coordenadas iguales, entonces lanza error 2
    findall([A,B],acido(A,B),L),	%Lista en L todos los pares (X,Y) 
    sort(L, R),						%Elimina replicados, nueva lista.
    length(L, NUM1),				%Obtiene tama�os de las listas
    length(R, NUM2),				%
    NUM1 =:= NUM2 -> true;			%Compara si son o no iguales las listas
									%es decir, si hay o no acidos repetidos
	%como se detecto error, se da cuenta de ello en el archivo de
	%salida y se aborta la ejecucion.
	tell('salida.txt'),
	write('Error 2: Un acido en la misma posicion que otro acido.'),
	told,
	abort.
% ===================================================================

%=============================ERROR 3 ==============================
error3:-
	%listo cada componente (X,Y) tanto de los acidos como de los 
	%bloques. asi recursivamente comparo con el limite de la bodega
	%de manera que si se encuentra una componente mayor a ese limite
	%se lanza error 3.
	%Se utiliza findall para listar todas las componentes y
	% en cada lista se aplica verificarCabezas.
	findall(Limite,dimension(Limite),LimiteBodega),  %Tama�o de la bodega
	findall(BloqueX,bloque(BloqueX,_),XdeLosBloques),%Lista por coordenadas X
	verificarCabezas(LimiteBodega,XdeLosBloques),	 %Verifica si esta dentro del margen
	findall(BloqueY,bloque(_,BloqueY),YdeLosBloques),%Lista por coordenadas Y
	verificarCabezas(LimiteBodega,YdeLosBloques),	 %Verifica si esta dentro del margen
	findall(AcidoX,acido(AcidoX,_),XdeLosAcidos),	 %Lista por coordenadas X
	verificarCabezas(LimiteBodega,XdeLosAcidos),	 %Verifica si esta dentro del margen
	findall(AcidoY,acido(_,AcidoY),YdeLosAcidos),	 %Lista por coordenadas Y.
	verificarCabezas(LimiteBodega,YdeLosAcidos).	 %Verifica si esta dentro del margen

verificarCabezas(Elemento,[Cabeza|Cola]):- 
%recursivamente se comparara elemento a elemento, si alguno es mayor
%a 'Elemento' quiere decir que esta fuera de los limites de la bodega
%por lo tanto error.
	(Elemento-1<Cabeza)->true,
	%Como se detecto el error se da cuenta de ello en el archivo de salida
	%adem�s se aborta la ejecucion.
	tell('salida.txt'),
	write('Error 3: Un acido o bloque fuera de los limites de la bodega.'),
	told,
	abort;
	verificarCabezas(Elemento,Cola).

verificarCabezas(_,[]). %condicion de borde parar la recursividad.
% ==================================================================

% ============================ ERROR 4 =============================

error4:-
	%Listo todas las coordenadas (X,Y) de los �cidos y, uno a uno,
	%reviso si hay un boque cerca. De haber uno alrededor se lanzar�
	%error 4.
	findall([X,Y],acido(X,Y),ACIDOS),
	recorrerACIDOS(ACIDOS).
	
recorrerACIDOS([[XB|YB]|Siguientes]):-
	%Si se detecta un bloque en el rango de restricci�n del �cido
	%o sobre el, se detectar� error 4.
	bloque(X,Y), X is XB, Y is YB,tell('salida.txt'),write('Error 4: Un bloque posicionado en el rango de restriccion del �cido.'),told,abort;
	bloque(X,Y), X is XB+1, Y is YB,tell('salida.txt'),write('Error 4: Un bloque posicionado en el rango de restriccion del �cido.'),told,abort;
	bloque(X,Y), X is XB+1, Y is YB+1,tell('salida.txt'),write('Error 4: Un bloque posicionado en el rango de restriccion del �cido.'),told,abort;
	bloque(X,Y), X is XB, Y is YB+1,tell('salida.txt'),write('Error 4: Un bloque posicionado en el rango de restriccion del �cido.'),told,abort;
	bloque(X,Y), X is XB-1, Y is YB+1,tell('salida.txt'),write('Error 4: Un bloque posicionado en el rango de restriccion del �cido.'),told,abort;
	bloque(X,Y), X is XB-1, Y is YB,tell('salida.txt'),write('Error 4: Un bloque posicionado en el rango de restriccion del �cido.'),told,abort;
	bloque(X,Y), X is XB-1, Y is YB-1,tell('salida.txt'),write('Error 4: Un bloque posicionado en el rango de restriccion del �cido.'),told,abort;
	bloque(X,Y), X is XB, Y is YB-1,tell('salida.txt'),write('Error 4: Un bloque posicionado en el rango de restriccion del �cido.'),told,abort;
	%Recorro recursivamente la lista de coordenadas.
	recorrerACIDOS(Siguientes). 
%Condici�n de borde para la recursi�n.
recorrerACIDOS([]).

% =================================================================	
	
% ============================ ERROR 5 ============================	
error5:-
%Se lista todas las ocurrencias de dimension(N).
%luego se guardan en una lista. Si la cardinalidad, es decir, el
%numero de elementos de la lista es distinta de 1, se lanzara error 5.
	listaDimensiones(L),		%Se listan todas las ocurrencias de dimension(L)
	length(L,M),M=:=1->true;	%Si el tama�o de la lista obtenida anteriormente
								%es mayor a 1, se lanza error 5
	%Como se encontro error, se procede a informar mediante el archivo de 
	%salida y se aborta la ejecucion
	tell('salida.txt'),
	write('Error 5: El archivo contiene m�s de una dimensi�n.'),
	told,
	abort.

%Lista todas las ocurrencias de dimension y las guarda en L.
listaDimensiones(L):-
	findall(A,dimension(A),L).	%Se buscan todas las ocurrencias de dimension(A)
								%cada ocurrencia se lista en L.

% ==================================================================

% ======================== Recorrer Bodega =========================

% obtiene el tamano de la matriz
tam_camino(N):-
    findall(A, dimension(A),N).

% crear... una regla (?), con la posicion visitada
visitar(X,Y):-
    assert(visitado(X,Y)).

% quita la posicion visitada
no-visitado(X,Y):-
    retract(visitado(X,Y)).

% L: una lista C: la cabeza, primer elemento de la lista
cabeza(L,C):-
    L = [H | _],
    C = H.

% para comprobar si se llego al final
esobjetivo(X,Y):-
    findall(A ,objetivo(A), T),
    cabeza(T, HEAD),
    X =\= HEAD -> Y =\= HEAD.   % si no se llego al final se continua, si ambos son diferentes
                                % esto dara falso y podra continuar la ejecucion

% obtiene el tama�o del mapa
obtener_limite(L):-
    tam_camino(N),
    cabeza(N, C),
    Lim is C - 1,
    L = Lim.

% crea regla en las posiciones de los bloques
% L: la lista de bloques en pares de cordenada
% Los agrega recursivamente
% Se agrega para despues consultar... aunque parece que esta de mas :)
marcar_bloque(L):-
    L = [H | T], % H: el primer elemento de la lista; T: los elementos restantes
    assert(bloque_pos(H)),
    marcar_bloque(T).

% se mueve a las posiciones X e Y
% Se puede hacer que de las posiciones X e Y se mueva a una posicion correcta
mover(X,Y):-
    visitar(X,Y),
    esobjetivo(X,Y),
    NX is X +1, NY is Y + 1,

    mover(NX, NY).

% busca los bloques y los agrega
agregar_bloques:-
    findall([X,Y], bloque(X,Y),  Bloq),

    marcar_bloque(Bloq).



recorrer:-
    agregar_bloques,
    obtener_limite(Lim),
    assert(objetivo(Lim)),
    X is 0, Y is 0,
    visitar(X,Y),

    NX is X + 1, NY is Y + 1, 
    NX < Lim, NY < Lim,         % para saber cuando se pasa de largo
    mover(NX, NY),
    findall([X,Y], visitado(X,Y), T),
    length(T, NUM),
    write(NUM).


    


    


% ==================================================================			
					
% =========================== Principal ============================	
	
%Principal, encadena todo tanto b�squeda de errores como si
%es posible pasar de (0,0) a (N,N).
% No utiliza par�metros, al momento de consultarle a prolog, se utiliza
% 'consultar.'
consultar:-
	consult(plano),
	error5,
	error1,
	error2,
	error3,
	error4.
	

% ==================================================================
